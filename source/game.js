window.onload = () => {

  const jwt = localStorage.getItem('jwt');
  if (!jwt) {
    location.replace('/login');
  } else {
    const socket = io.connect('http://localhost:3000');
    
    const listUsers = document.querySelector('.users-in-game');
    const textEntered = document.querySelector('#entered');
    const textEnter = document.querySelector('#enter');
    const textWillEnter = document.querySelector('#will-enter');
    const divText = document.querySelector('.text');
    const timerField = document.querySelector('.time');
    let userScore = 0;

    function startGame() {
      window.addEventListener('keydown', e => {
        if (textWillEnter.textContent.length === 30) {
          socket.emit('ratingPlayers');
        } 
        if (textWillEnter.textContent == '') {
          socket.emit('gameOver', { token: jwt });
        } else {
          if (e.key == textEnter.textContent) {
            userScore++;
            socket.emit('calcUserScore', {score: userScore, token: jwt});
            textEntered.innerHTML += textEnter.textContent;
            textEnter.innerHTML = textWillEnter.textContent.substring(0,1);
            textWillEnter.innerHTML = textWillEnter.textContent.substring(1);
          }
        }
      });
    }

    function setText(race) {
      textEnter.innerHTML = race.substring(0,1);
      textWillEnter.innerHTML = race.substring(1);
      startGame();
    }
    
    socket.emit('connectedNewUser', { token: jwt });
    
    socket.on('addNewUser', payload => {
      const { race, usersList } = payload.gameReady;
      listUsers.innerHTML = '';
      for (let i = 0; i < usersList.length; i++) {
        const newLi = document.createElement('li');
        let userName = usersList[i];
        newLi.innerHTML = userName;
        socket.on('editUserScore', payload => {
          if (payload.name == userName) {
            newLi.innerHTML = `${userName} <progress value="${payload.score}" max="${race.length}"></progress>`;
          }
        });
        listUsers.appendChild(newLi);
      }
      
      setText(race);
    });

    const commentField = document.querySelector('.comment');

    socket.on('commentAccost', payload => {
      let listPlayers = `А тим часом ми можемо бачити тих хто 
                        участвує в гонці: ${payload.users[0]}`;
      for (let i = 1; i < payload.users.length; i++) {
        listPlayers += `, також ${payload.users[i]}`;  
      }
      commentField.innerHTML = `<span>${payload.accost}</span>
                                <span>${listPlayers}</span>`;
    });

    // Proxy (ES2015) - використання патернів
    function ratingUsers(payload) {
      const listWithRatign = payload.ratingPlayers;
      const racers = {};
      for (let i = 0; i < listWithRatign.length; i++) {
        racers[i+1] = listWithRatign[i].user;
      }
  
      let ridersWhoParticipate = new Proxy(racers, {
        get(target, phrase) {
          if (phrase in target) {
            return target[phrase];
          } else {
            return '- упс, оце так і поворот - даний учасник бастує і тому в нинішньому заїзді ми його не зможемо побачити';
          }
        }
      })

      return ridersWhoParticipate;
    }

    socket.on('rating30Leters', payload => {
      const racers = ratingUsers(payload);
      commentField.innerHTML = `До фінішу залишилось зовсім небагато 
                                і схоже що першим його може перетнути 
                                ${racers[1]} на своєму білому 
                                BMV. Друге місце може залишитись 
                                ${racers[2]}, а третє скоріш за все 
                                дістанеться ${racers[3]}. 
                                Але давайте дочекаємось фінішу.`;
    });  

    socket.on('rating30sec', payload => {
      const racers = ratingUsers(payload);
      commentField.innerHTML = `Першим до фінішу летить ${racers[1]} 
                                на своєму білому BMV, на другому 
                                місці прямує на Audi ${racers[2]}, 
                                і на кінець тверде третє на Volvo 
                                тримає ${racers[3]}.`;
    });

    socket.on('gameOverForUser', payload => {
      divText.innerHTML = `came to the finish ${payload.name}`;
    });

    socket.on('gameOverForOther', payload => {
      if (payload.whoFinished === 'Останім') {
        let finishResult = `Рейтинг учасників:`;
        for (let i = 0; i < payload.finishList.length; i++) {
          finishResult += `<br>${i+1} --- ${payload.finishList[i]}`
        }
        commentField.innerHTML = `<span>${payload.whoFinished} фінішну пряму пересікає ${payload.name}</span>
                                  <span>${finishResult}</span>`;
      } else {
        commentField.innerHTML = `${payload.whoFinished} фінішну пряму пересікає ${payload.name}`;
      }  
    });

  }

}